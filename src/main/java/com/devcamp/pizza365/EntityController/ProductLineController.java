package com.devcamp.pizza365.EntityController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.EntityRespository.iProductLineRespository;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;

@RestController
@RequestMapping("productline")
public class ProductLineController {
    
    @Autowired
    iProductLineRespository iProductLine ;

    @GetMapping("/all")
    public ResponseEntity <List <ProductLine>> getProducts() {
        
        try {
            List<ProductLine> listProduct = new ArrayList<ProductLine>();
            iProductLine.findAll().forEach(listProduct :: add);

            if(listProduct.size() == 0){
                return new ResponseEntity<List <ProductLine>>(listProduct, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <ProductLine>>(listProduct, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   
    @GetMapping("/detail")
    public ResponseEntity<Object> getProductById(@PathVariable(name = "id", required = true) int id) {
        Optional<ProductLine> ProductFounded = iProductLine.findById(id);
        if (ProductFounded.isPresent())
            return new ResponseEntity<Object>(ProductFounded, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createProduct(@RequestBody ProductLine productLine) {
        Optional<ProductLine> ProductData = iProductLine.findById(productLine.getId());
        try {
        if(ProductData.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" Drink already exsit  ");
        }
        productLine.setProductLine(productLine.getProductLine());
        productLine.setDescription(productLine.getDescription());
      
    
        ProductLine saveProduct = iProductLine.save(productLine);
        return new ResponseEntity<>(saveProduct, HttpStatus.CREATED);
    }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateProductById(@PathVariable (name = "id")int id , @RequestBody ProductLine productLine) {
        Optional<ProductLine> ProductData = iProductLine.findById(id);
        if (ProductData.isPresent()) {

			ProductLine saveProduct = ProductData.get();
            productLine.setProductLine(productLine.getProductLine());
            productLine.setDescription(productLine.getDescription());
          
            
            try {
                return ResponseEntity.ok(iProductLine.save(saveProduct));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Can not execute operation of this Entity"+ e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object> (null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ProductLine> deleteProductById(@PathVariable (name = "id") int id) {
        try {
			iProductLine.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
}


